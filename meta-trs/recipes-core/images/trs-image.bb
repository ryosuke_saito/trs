# Copyright (c) 2022, Arm Limited.
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "TRS image"
LICENSE = "MIT"

SRC_URI = "file://testimage_data.json"
DEPENDS += "e2fsprogs-native swtpm-native"

inherit core-image image-buildinfo extrausers deploy

# Enable EFI booting configurations in meta-ledge-secure layer
inherit image-efi-boot

# meta-virtualization/recipes-containers/k3s/README.md states that K3s requires
# 2GB of space in the rootfs to ensure containers can start
#
# OpenAD Kit Demo Docker image requires 8 GB

IMAGE_FEATURES += "\
    bash-completion-pkgs \
    debug-tweaks \
    ssh-server-openssh \
"

# uses meta-ledge-secure secure boot
IMAGE_INSTALL += "\
    ${@bb.utils.contains("DISTRO_FEATURES", "wifi", "packagegroup-base-wifi", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "sota", "ostree-booted", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "grub", "packagegroup-ledge-grub", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "optee", "packagegroup-ledge-optee", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "packagegroup-ledge-tpm-lava", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "packagegroup-security-tpm2", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tsn", "packagegroup-ledge-tsn", "", d)} \
    packagegroup-security-parsec \
    aws-cli \
    bash \
    bash-completion-extra \
    ca-certificates \
    composefs \
    docker-moby \
    efivar \
    ethos-u-driver-tests \
    firmware-imx-easrc-imx8mn \
    firmware-imx-xcvr-imx8mp \
    fsverity-utils \
    fwts \
    fwupd \
    fwupd-efi \
    greengrass-bin \
    imx-ethos-u-firmware \
    k3s-server \
    kernel-module-xen-blkback \
    kernel-module-xen-gntalloc \
    kernel-module-xen-gntdev \
    kernel-module-xen-netback \
    kernel-modules \
    kmod \
    lava-systemd-metrics \
    libp11 \
    linux-firmware-imx-sdma-imx7d \
    linux-firmware-rtl8188 \
    linux-firmware-rtl8192cu \
    linux-firmware-sd8887 \
    linux-firmware-sd8997 \
    ltp \
    net-tools \
    opensc \
    optee-test \
    procps \
    qemu-keymaps \
    qemu-system-i386 \
    soafee-test-suite \
    strace \
    sudo \
    systemd-analyze \
    wget \
    xen-tools \
    xz \
"

EXTRA_IMAGEDEPENDS += "xen"

WKS_FILE_DEPENDS += "ledge-initramfs"
INITRAMFS_IMAGE = "ledge-initramfs"
do_image_wic[depends] += "ledge-initramfs:do_image_complete"
WKS_FILE = "ledge-secure.wks.in"
WKS_FILE:sota = "ledge-secure-sota.wks.in"
IMAGE_BOOT_FILES = "${KERNEL_IMAGETYPE}"

EXTRA_USERS_PARAMS:prepend = "\
    useradd -p '' ewaol; \
    useradd -p '' test; \
    useradd -p '' trs; \
    groupadd trs; \
    groupadd sudo; \
    usermod -aG docker,trs,sudo ewaol; \
    usermod -aG docker,sudo trs; \
"

IMAGE_FSTYPES = "wic wic.bz2 wic.qcow2 wic.bmap"
IMAGE_FSTYPES:append:sota = " ostreepush garagesign garagecheck"
# Default bootstrap image file system is ext4
IMAGE_FSTYPES:append:sota = " ota-ext4"
#IMAGE_FSTYPES:append:sota = " ota-btrfs"

IMAGE_CMD:ostree:prepend () {
    dir_lst="backups lib/arpd lib/dbus lib/misc lib/sudo/lectured lib/sudo"
    dir_lst="${dir_lst} lib/systemd/backlight lib/systemd lib/tpm"
    dir_lst="${dir_lst} spool/cron spool/mail spool volatile"

    cd ${IMAGE_ROOTFS}/var
    for dir in $dir_lst; do
        ostree_rmdir_helper "${dir}"
    done
    cd -
}

IMAGE_CMD:ota:append () {
    mkdir -p \
        ${OTA_SYSROOT}/ostree/deploy/${OSTREE_OSNAME}/var/rootdirs/opt \
        ${OTA_SYSROOT}/ostree/deploy/${OSTREE_OSNAME}/var/usrlocal

    cp -a ${IMAGE_ROOTFS}/opt/cni ${OTA_SYSROOT}/ostree/deploy/${OSTREE_OSNAME}/var/rootdirs/opt
    cp -a ${IMAGE_ROOTFS}/opt/ltp ${OTA_SYSROOT}/ostree/deploy/${OSTREE_OSNAME}/var/rootdirs/opt
    cp -a ${IMAGE_ROOTFS}/usr/local/bin ${OTA_SYSROOT}/ostree/deploy/${OSTREE_OSNAME}/var/usrlocal
}

# re-enable SRC_URI handling, it's disabled in image.bbclass
python __anonymous() {
    d.delVarFlag("do_fetch", "noexec")
    d.delVarFlag("do_unpack", "noexec")
}

# setup testimage.bbclass to execute oeqa runtime tests
TEST_RUNQEMUPARAMS = "slirp nographic novga"

# setup for poky/scripts/runqemu:
QB_MACHINE = "-machine virt,virtualization=on,secure=on"

# currently crashes tf-a/u-boot, https://linaro.atlassian.net/browse/TS-435
# overwrite variable from poky/meta/conf/machine/qemuarm64.conf since removing
# "-device usb-kbd" with :remove is not possible (removes all "-device" and
# "usb-kbd" strings)
QB_OPT_APPEND = "-device qemu-xhci -device usb-tablet"

QB_OPT_APPEND += "-machine acpi=off"

QB_MEM = "-m 2048"
QB_DEFAULT_FSTYPE = "wic"
QB_DEFAULT_BIOS = "flash.bin"
QB_FSINFO = "wic:no-kernel-in-fs"
QB_ROOTFS_OPT = ""
QB_CPU = "-cpu cortex-a57"

# kernel is in the image, should not be loaded separately
QB_DEFAULT_KERNEL = "none"

# setup SW based TPM for testing, note socket file path has 107 character
# length limitations from sockaddr_un
QB_SETUP_CMD = " \
   set -ex; pwd; which swtpm; swtpm --version; which swtpm_setup; \
   test -d '${IMAGE_BASENAME}_swtpm' || ( mkdir -p '${IMAGE_BASENAME}_swtpm' && \
       swtpm_setup --reconfigure --tpmstate '${IMAGE_BASENAME}_swtpm' --tpm2 --pcr-banks sha256 --config $(dirname $( which swtpm ) )/../../etc/swtpm_setup.conf ) && \
   test -f '${IMAGE_BASENAME}_swtpm/tpm2-00.permall' && \
   swtpm socket --tpmstate dir='${IMAGE_BASENAME}_swtpm' \
         --ctrl type=unixio,path='${IMAGE_BASENAME}_swtpm/swtpm-sock' \
         --flags startup-clear \
         --log level=30 --tpm2 -t -d \
"

QB_OPT_APPEND += "-chardev socket,id=chrtpm,path='${IMAGE_BASENAME}_swtpm/swtpm-sock' -tpmdev emulator,id=tpm0,chardev=chrtpm -device tpm-tis-device,tpmdev=tpm0"

# Due to rootfs encryption, boot to serial login prompt may
# take really long on a loaded machine
TEST_QEMUBOOT_TIMEOUT = "2000"

# set this correclty for testimage.bbclass, will be wrong for
# testexport.bbclass but that can be overwritten using command line
# argument:
# ./oe-test runtime --target-type "simpleremote"
TEST_TARGET = "qemu"

# keep ping and ssh first since most tests depend on them
# and this variable used for test execution order too
# https://docs.yoctoproject.org/singleindex.html#term-TEST_SUITES
TEST_SUITES = "\
    ping \
    ssh \
    tpm \
    opteetest \
    date \
    df \
    dtc \
    parselogs \
    ptest \
    python \
    rtc \
    soafeetestsuite \
    systemd \
    tpm \
"

do_deploy() {
    # to customise oeqa tests
    mkdir -p "${DEPLOYDIR}"
    install "${WORKDIR}/testimage_data.json" "${DEPLOYDIR}"
}

do_deploy:append:qemuarm() {
    # workaround meta-ts firmware link for testimage.bbclass
    ( cd "${DEPLOYDIR}" && \
      ln -sf ../../../../tmp_tsqemuarm-secureboot/deploy/images/tsqemuarm-secureboot/flash.bin flash.bin \
    )
}

do_deploy:append:qemuarm64() {
    # workaround meta-ts firmware link for testimage.bbclass
    ( cd "${DEPLOYDIR}" && \
      ln -sf ../../../../tmp_tsqemuarm64-secureboot/deploy/images/tsqemuarm64-secureboot/flash.bin flash.bin \
    )
}
# do_unpack needed to run do_fetch and do_unpack which are disabled by image.bbclass.
addtask deploy before do_build after do_rootfs do_unpack

# test depend on native tools like qemu and swtpm, fixes rebuild with full caching
do_testimage[depends] += "swtpm-native:do_populate_sysroot"
# leave native sysroot around so we can use swtpm from there outside of bitbake builds
RM_WORK_EXCLUDE_ITEMS += "recipe-sysroot-native"

# export tests automatically at image build time
addtask testexport before do_build after do_image_complete
