FILESEXTRAPATHS:prepend:anaconda := "${THISDIR}/files:"

SRC_URI:append:anaconda = " \
    file://stage2-download \
"

do_install:append:anaconda() {
    # Download stage2 image if specified before 80-setup-live script.
    install -m 0755 ${WORKDIR}/stage2-download ${D}/init.d/70-stage2-download
}

FILES:${PN}:append:anaconda = " \
    /init.d/70-stage2-download \
"
