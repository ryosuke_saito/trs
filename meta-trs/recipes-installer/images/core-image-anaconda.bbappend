PCBIOS:qemuarm64 = "0"

populate_live:append:qemuarm64() {
    # Truncate rootfs.img since u-boot can't pass it to Linux anyway in the
    # current implementation. Instead, 'stage2' boot parameter will be used
    # to download it from a HTTP or NFS server.
    if [ -s "${ROOTFS}" ]; then
        : > $1/rootfs.img
    fi
}

build_iso:prepend:qemuarm64() {
    # isohybrid returns 1 while successful, which means the task failed.
    # To workaround this, disable the exception at the beginning.
    set +e
}

KICKSTART_DIR := "${THISDIR}/kickstart"
KICKSTART_FILE = "${KICKSTART_DIR}/trs.ks.template"
