# Enable syslinux for ARM due to the anaconda dependency, while we neigther
# acutually build nor use them.
COMPATIBLE_HOST:class-target = '(x86_64|i.86|arm|aarch64).*-(linux|freebsd.*)'

do_install:class-target:aarch64() {
	# Don't run oe_runmake since syslinux doesn't support ARM.

	# We don't need them but just deploy prebuilt binaries for dependencies.
	install -d ${D}${datadir}/syslinux/
	install -m 644 ${S}/bios/core/ldlinux.sys ${D}${datadir}/syslinux/
	install -m 644 ${S}/bios/core/ldlinux.bss ${D}${datadir}/syslinux/

        # Add dummy isolinux.bin to workaroud an iso packaging error.
	touch ${D}${datadir}/syslinux/isolinux.bin
}
