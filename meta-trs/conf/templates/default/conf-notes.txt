### Shell environment set up for builds. ###

To build for all target machines, please copy multiconfig setup:

 $ cp -a ../meta-ts/meta-trustedsubstrate/conf/templates/multiconfig conf/
 $ cp -a ../meta-trs/conf/templates/multiconfig conf/

Then you can run 'bitbake <target>'

Common targets are:

    trs-image (default MACHINE=trs-qemuarm64)
    mc:trs-qemuarm64:trs-image (using multiconfig)

Other commonly useful commands are:
 - 'devtool' and 'recipetool' handle common recipe tasks
 - 'bitbake-layers' handles common layer tasks
 - 'oe-pkgdata-util' handles common target package tasks
