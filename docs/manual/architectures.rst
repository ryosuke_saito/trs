..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

####################
System Architectures
####################
From a high level, TRS provides two main variants, the baremetal variant and the
virtualization variant. Both are built from the same sources with minor
modifications.

Baremetal and virtualization are two independent software deployment strategies
for computer systems. Baremetal refers to the installation and running of an
operating system without any intervening software layers. In other words, the
OS directly interacts with the machine's physical hardware, including the CPU,
memory, storage, and networking devices etc.

Virtualization, on the other hand, involves the creation of one or more virtual
machines (VMs) that run on top of a hypervisor, which is a software layer that
abstracts the underlying physical hardware and exposes it to the VMs as a set of
virtual hardware components. Each VM have the ability to run its own operating
system, which interacts with the hypervisor's virtual hardware. This
essentially means that it is possible to run several different operating
systems concurrently on the same physical hardware.

So, baremetal systems communicate directly with the real hardware, whereas
virtualized systems communicate with the hypervisor's virtual hardware.
Ultimately, the decision between bare metal and virtualization is determined by
the individual use case and system requirements. Virtualization is often used in
data centers and other business contexts where flexibility, administration, and
effective resource utilization are essential. But there is a trend where other
markets started looking at running virtualized environments. Automotive is one,
but we've even seen that mobile handset have use cases where they want to
leverage virtual environments for various workloads.

Since TRS have the ability act as both baremetal and a virtualized architecture,
with and without using container technologies, we believe that TRS is well
suited for a lot of use cases and scenarios.

Baremetal architecture
**********************
Conceptually, TRS has code running in every architectural layer. With an Armv8-A
device, this means that code will run on all exception levels on both the
non-secure and secure (TrustZone) side. The diagram below does not show a
typical image with secure and non-secure sides and exception levels; rather, it
is intended to provide a quick overview of the kinds of building blocks we are
likely to see being used in an TRS baremetal setup.

At the very top, we have (OCI) containerized workloads. Linaro will be able to
provide workload applications designed to solve specific use cases; these are
often included in Arm and Linaro Blueprint offerings. Additional container
images might be directly retrieved from image registries.

The next layer consists of standard Linux tools and apps, which are often
user-space applications or libraries that provide and abstract diverse hardware.
Parsec, for example, is intended to abstract security hardware such as TEE
environments, TPMs, and HSMs, among others. On the security side, we also find
libraries that enable direct communication with OP-TEE. This conforms to
the `GlobalPlatform`_ Client and Internal API standards. This layer also
contains the numerous test tools and test suites required to guarantee API
correctness, device stability and robustness.

The last software layer consists of the Linux operating system and low-level
hardware. We've also included the bootloader, which is mostly utilized during
boot. Yet, even bootloader code has portions that remain resident after the main
operating system gains control. For example, the `secure monitor`_ code
(``BL31``) never stops running and the same is true for the `OP-TEE OS`_ code,
which provides security features to the main OS for various use cases.

Then at the bottom we've included a hardware layer showing the type of devices
that we support in TRS. This is something that will change over time. For an
up-to-date list, please have a look at our `CI matrix`_ that is continuously
updated and shows all the devices that we test on a regular basis.


.. image:: ../images/user-guide-baremetal-arch-01.png
  :alt: TRS baremetal architecture

Virtualization architecure
**************************
As for the virtualization architecture, TRS supports hardware virtualization by
using Xen, which is a well-known type-1 hypervisor implementation. If we look
at the block diagram below, we mostly find the same bits and pieces as we saw
in the block diagram for the baremetal architecture. The major difference is
the hypervisor layer that has been included between the hardware and the rest
of the software. The hypervisor layer is responsible for managing the virtual
machines and allocating resources to them. It allows multiple operating systems
to run on the same physical hardware simultaneously. As shown in the block
diagram above, you can also use container applications to run separate
workloads if guest separation isn't enough.

.. image:: ../images/user-guide-virtualization-arch-01.png
  :alt: TRS baremetal architecture


.. _CI matrix: https://gitlab.com/Linaro/blueprints/ci/-/blob/main/README.md
.. _defines: https://ewaol.docs.arm.com/en/kirkstone-dev/manual/build_system.html#customization
.. _GlobalPlatform: https://globalplatform.org/specs-library/?filter-committee=tee
.. _secure monitor: https://www.arm.com/technologies/trustzone-for-cortex-a/tee-and-smc
.. _OP-TEE OS: https://optee.readthedocs.io/en/latest/architecture/core.html
