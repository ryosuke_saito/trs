..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

############
Contributing
############
As an open source project, TRS welcomes contributions from anyone willing to
submit patches that conform to the licensing rules. The primary trs.git
repository uses the MIT license (see :ref:`License`). Also, for the majority of
the remaining TRS projects, changes should be provided directly to the upstream
source and not via TRS. TRS will adopt them when we update our manifest files to
use more recent versions of the sub-projects in TRS.

Contribution Guidelines
***********************
The way to contribute is pretty much the same as what is usually done in open
source projects. All patches are integrated via GitLab Merge Requests and
therefore, we do not accept ``*.patch`` patches sent via email. That's because
we want to run various regression tests on the supported devices and these
tests are typically triggered by GitLab Merge Requests.

Forking
=======
To be able to make a contribution, you need a `GitLab account`_, hence start out
by creating that. Once that is completed, you should fork the git where you
intend to make changes. In the GitLab web interface you find a "Fork" button up
to the right when you've selected a git project. Once pressed, you'll end up
with your own copy of the git at your own GitLab account.

Merge Request
=============
After forking the git, you'll clone the git from your own GitLab account, make
your changes to the project and once you're done with the changes it's time to
submit the patches. Before sending your code make sure changes have been tested
locally (``make test``), squashed patches into a patch series that makes sense,
written a good commit message etc. The `merge request`_ itself can be done via
the GitLab web interface.

Commit messages
===============
The subject line should explain what the patch does as precisely as possible. It
is usually prefixed with keywords indicating which part of the code is affected,
but not always. Avoid lines longer than 80 characters.

The commit description should give more details on what is changed and explain
why it is done. Indication on how to enable and use a particular feature can be
useful too. Try to limit line length to 72 characters, except when pasting some
error message (compiler diagnostic etc.). Long lines are allowed to accommodate
URLs, too (preferably use URLs in a Fixes: or Link: tag).

..
 # TODO: We haven't required "Signed-off-by" so far.
 #The commit message must end with a blank line followed by some tags, including
 #your Signed-off-by: tag. By applying such a tag to your commit, you are
 #effectively declaring that your contribution follows the terms stated by
 #Developer Certificate of Origin (in the DCO section there is also a complete
 #example).

When it makes sense, we encourage to use other tags as well, such as:

- Tested-by: Teste R <teste@r.com>
- Acked-by: Acke R <acke@r.com>
- Suggested-by: Suggeste R <suggeste@r.com>
- Reported-by: Reporte R <reporte@r.com>

When citing a previous commit, whether it is in the text body or in a Fixes:
tag, always use the format as shown in the example below, that is 12 hexadecimal
digits prefix of the commit SHA1, followed by the commit subject in double
quotes and parentheses.

.. code:: bash

   crypto: RSA driver fix

   This fixes e1c70d7c88ab ("crypto: drivers: se050: fix rsa encrypt/decrypt")
   ...

Review feedback
~~~~~~~~~~~~~~~
It is very likely that you will get review comments from other TRS users asking
you to fix certain things etc. When fixing review comments, do:

- Add fixup patches on top of your existing branch. Do not squash and force
  push while fixing review comments.

- When all comments have been addressed, just write a simple messages in the
  comments field saying something like “All comments have been addressed”. By
  doing so you will notify the maintainers that the fix might be ready for
  review again.

- When all comments have been addressed, once again rebase and squash patches
  into a patch series that make sense.

.. _GitLab account: https://gitlab.com/users/sign_up
.. _merge request: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
